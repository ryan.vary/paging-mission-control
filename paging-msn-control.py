import sys
import json
from collections import deque
from datetime import datetime, timedelta

#Component
#Methods: determineCriticalReading, setMeasurement, setTimeStamp, slideWindow, getComponent, getSeverity
class Component:
    def __init__(self, component, red_hl, red_ll):
        self.componentType = component
        self.measurements = deque()
        self.redHigh = float(red_hl)
        self.redLow = float(red_ll)
        self.timeStamps = deque()
        self.windowWarnings = 0

    def determineCriticalReading(self, timestamp, measurement):
        m = float(measurement)
        if self.componentType == 'TSTAT' and m > self.redHigh or self.componentType == 'BATT' and m < self.redLow:
            self.setMeasurement(m)
            self.setTimeStamp(timestamp)
            self.windowWarnings += 1
            return 1
        else: 
            return 0
    
    def setMeasurement(self, measurement):
        self.measurements.append(float(measurement))
        
    def setTimeStamp(self, timeStamp):
        t = datetime.strptime(timeStamp, '%Y%m%d %H:%M:%S.%f') 
        self.timeStamps.append(t)
    
    def getComponent(self): 
        return self.componentType

    def getSeverity(self): 
        if self.componentType == 'TSTAT':
            return 'RED HIGH'
        else: 
            return 'RED LOW'

    def slideWindow(self):
        self.measurements.popleft()
        self.timeStamps.popleft()
        self.windowWarnings -= 1

#Satellite
#Methods: addComponent, getSatId  
class Satellite:
    def __init__(self, satId):
        self.satId = int(satId)
        self.components = {}

    def addComponent(self, component, rh, rl):
        self.components[component] = Component(component, rh, rl)

    def getSatId(self):
        return self.satId

#generateAlert
#Step 1: Determine timestamp of oldest anamolous telemetry reading in window
#Step 2: Generate message
#Step 3: Remove message from window since we only emit 1 reading for any anomalous event (ie. if another anomalous event occured within a 5 min window, the next message in the window would be emitted); the Component class' slideWindow() method moves the window to remove this message
def generateAlert(sat, component): 
    timestamp = component.timeStamps[0]
    msg = { 
        "satelliteId": sat.getSatId(),
        "severity": component.getSeverity(),
        "component": component.getComponent(),
        "timestamp": timestamp.strftime("%Y-%m-%dT%H:%M.%S.%f")[:23] + "Z"
    }
    alert = json.dumps(msg)
    print(alert)
    component.slideWindow()

#determineAlert - should an alert message be generated? 
#Step 1: Obtain the timestamp associated with the most recent anamolous telemetry reading
#Step 2: Determine if anamlous telemetry readings are within a 5 min window; the Component class' slideWindow() moves the window
#Step 3: If the number of warnings within the 5 min window are > 2, generate an alert message; handled by generateAlert method
def determineAlert(satellite, component): 
    lastMeasuredTime = component.timeStamps[-1]
    delta = timedelta(seconds=300)
    j = 0
    while (abs(lastMeasuredTime - component.timeStamps[j]) > delta):
        component.slideWindow()
        j+=1
    if component.windowWarnings > 2:
        generateAlert(satellite, component)         

#Driver for code.  
#Analysis done line-by-line (per event) 
#Sliding window implemented to remove previous anomalous telemetry readings from analysis
#Step 1: Parse messages & discard yellow readings
#Step 2: Determine if satID and componentType has been received in the message stream.  If it hasn't, instantiate the satellite and associated component.  
#Step 3: Determine if component's telemetry measurand is outside of nominal range; handled in Component class' determineCriticalReading method.
#Step 4: If the measurand is outside nominal range, determine if an alert message should be generated; hanlded in determineAlert method.
def main():
    satellites = {}
    if len(sys.argv) != 2:
        print("Usage: python3 paging-mission-control.py <filename>")
        return

    filename = sys.argv[1]

    try: 
        with open(filename, 'r') as readings:
            for line in readings:
                tokens = line.split("|")
                timeStamp = tokens[0]
                satId = tokens[1]
                redHigh = tokens[2]
                redLow = tokens[5]
                rawValue = tokens[6]
                componentType = tokens[7].strip("\n")

                if satId not in satellites:
                    satellites[satId] = Satellite(satId)
                    satellites[satId].addComponent(componentType, redHigh, redLow)

                elif satId in satellites and componentType not in satellites[satId].components:
                    satellites[satId].addComponent(componentType, redHigh, redLow)
                
                component = satellites[satId].components[componentType] 
                analysisRequired = component.determineCriticalReading(timeStamp, rawValue)
         
                if analysisRequired: 
                    determineAlert(satellites[satId], component)
    
    except FileNotFoundError:
        print("Error: File '{filename}' not found.")

if __name__ == "__main__":
   main() 
