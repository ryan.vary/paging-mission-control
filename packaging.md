# Solution
This solution was developed with Python3 (v3.8.10) on an Ubuntu 20.04 LTS machine.  

To install Python 3 on an Ubuntu 20.04 machine:
1. sudo apt-get install software-properties-common 
2. sudo add-apt-repository ppa:deadsnakes/ppa
3. sudo apt-get update
4. sudo apt-get install python3.8

The following core python packages are required to run the solution: 

* sys 
* json
* collections
* datetime 

To run: ```python3 paging-msn-control.py <filename>```

The solution's design is discussed in comments. `test.txt` was used to evaluate the solution's ability to handle corner cases.  
